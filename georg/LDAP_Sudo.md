# LDAP: Sudo

Prerequisites:

-   389 DS server
-   LDAP Directory Manager (or equivalent) permissions
-   SSSD client
-   Client root (or equivalent) permissions - ideally not only \`sudo\`
    permissions, in case you lock yourself out of \`sudo\`

### Verify the sudo schema is installed:

<div class="code panel pdl" style="border-width: 1px;">

<div class="codeContent panelContent pdl">

``` bash
# file (locate 60sudo.ldif)
/usr/share/dirsrv/schema/60sudo.ldif: ASCII text
```

</div>

</div>

### Create OU:

Create an organizational unit to house SUDOers settings using a GUI or
using ldapadd/ldapmodify. This is not documented here, but the LDIF
query should look something like this:

<div class="code panel pdl" style="border-width: 1px;">

<div class="codeContent panelContent pdl">

``` java
dn: ou=SUDOers,ou=syscid-system,dc=syscid,dc=com
changetype: add
ou: SUDOers
objectClass: organizationalUnit
objectClass: top
```

</div>

</div>

### Convert existing, local, sudoers to LDIF:

Convert an existing, local, sudoers file to an LDAP importable LDIF. Old
articles suggest the Perl script used for this should be included in the
\`sudo\` package, however that does not seem to be the case on the SUSE
systems I worked with - I dug the script out of deep parts of the
internet and attached it here - whether it is "original" I cannot tell.

Note that the SUDOERS_BASE environment variable needs to be filled with
the DN of the OU created above.

<div class="code panel pdl" style="border-width: 1px;">

<div class="codeContent panelContent pdl">

``` bash
# export SUDOERS_BASE=ou=SUDOers,ou=syscid-system,dc=syscid,dc=com
# echo $SUDOERS_BASE
ou=SUDOers,ou=syscid-system,dc=syscid,dc=com
# perl sudoers2ldif.pl /etc/sudoers | tee sudoers-389.ldif
# file sudoers-389.ldif
sudoers-389.ldif: ASCII text
```

</div>

</div>

#### Inspect the file for faulty lines:

In the `sudoers-389.ldif` file generated above, look for failed lines,
which may look similar to the following:

`parse error: Defaults!/usr/bin/sudoreplay !log_output`

`parse error: Defaults!REBOOT !log_output`

Remove them, in order to have a valid LDIF.

If the lines seem crucial to you, either correct the input sudoers file,
and run the script again, or try to manually add the settings in your OU
after the import.

### Import the LDIF:

<div class="code panel pdl" style="border-width: 1px;">

<div class="codeContent panelContent pdl">

``` bash
# ldapadd -xWD 'cn=Directory Manager' -f sudoers-389.ldif
```

</div>

</div>

### Verify the result:

<img src="attachments/5341350/5341353.png" height="150" /><img src="attachments/5341350/5341352.png" height="150" />

### Configure a client:

Add to / modify in `/etc/nsswitch.conf`:

sudoers:        sss

Add to / modify in /etc/sssd/sssd.conf:

sudo_provider = ldap ldap_sudo_search_base =
ou=SUDOers,ou=syscid-system,dc=syscid,dc=com

I read that a \`service\` entry should no longer be necessary, but am
adding it to my existing services for good measure:

services = nss, pam, ssh, sudo

<div class="pageSectionHeader">

## Attachments:

</div>

<div class="greybox" align="left">

<img src="images/icons/bullet_blue.gif" width="8" height="8" />
[SUDOers_Defaults.png](attachments/5341350/5341352.png) (image/png)  
<img src="images/icons/bullet_blue.gif" width="8" height="8" />
[SUDOers_OU.png](attachments/5341350/5341353.png) (image/png)  
<img src="images/icons/bullet_blue.gif" width="8" height="8" />
[sudoers2ldif.pl](attachments/5341350/5341355.pl) (application/x-perl)  

</div>
