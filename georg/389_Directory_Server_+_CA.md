# 389 Directory Server + CA

<div class="code panel pdl" style="border-width: 1px;">

<div class="codeContent panelContent pdl">

``` bash
# install
zypper in 389-ds openldap2-client

# base config
cat <<'EOF' >instance.inf
[general]
config_version = 2

[slapd]
instance_name = syscid
root_password = J0TMD8GdS5cNJD1jxg16WBtzr9SWWFVHzOpUoCn4QSlXkwKT

[backend-userroot]
create_suffix_entry = True
sample_entries = True
suffix = dc=syscid,dc=com
EOF

# init
dscreate from-file instance.inf

# stop
dsctl syscid stop

# modify /etc/ssl/openssl.cnf
...
[ policy_match ]
countryName             = optional
stateOrProvinceName     = optional
organizationName        = optional
...
database        = index.txt
serial          = serial
...

# create CA

mkdir /etc/pki/CA
cd /etc/pki/CA

# init first CA
touch index.txt
echo 01 > serial

# generate CA key
openssl genrsa -out ca.key 4096

# generate CA certificate
openssl req -new -x509 -days 365 -key ca.key -out ca.crt

# create extension config (for SANs)
cat <<'EOF' >server_cert_ext.cnf
[v3_ca]
basicConstraints = CA:FALSE
nsCertType = server
nsComment = "LDAP01 Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[ alt_names ]
DNS.1 = ldap.syscid.com
DNS.2 = ldap01.syscid.com
DNS.3 = dir.syscid.com
DNS.4 = dir01.syscid.com
DNS.5 = gaia.syscid.com
EOF

#
mkdir private
cd private/

# generate server key
openssl genrsa -out ldap.syscid.com.key 4096

# generate CSR
openssl req -new -key ldap.syscid.com.key -out ldap.syscid.com.csr

# generate server certificate
openssl ca -keyfile ca.key -cert ca.crt -in private/ldap.syscid.com.csr -out private/ldap.syscid.com.crt -extensions v3_ca -extfile server_cert_ext.cnf -outdir .

# wipe existing SLAPD NSS certificate database
rm /etc/dirsrv/slapd-syscid/*.db
certutil -d /etc/dirsrv/slapd-syscid/ -N

# export server certificate and server key to P12 bundle
openssl pkcs12 -export -in private/ldap.syscid.com.crt -inkey private/ldap.syscid.com.key -out /etc/dirsrv/slapd-syscid/ldap.syscid.com.p12 -name Server-Cert

# install server certificate in SLAPD certstore
pk12util -i /etc/dirsrv/slapd-syscid/ldap.syscid.com.p12 -d /etc/dirsrv/slapd-syscid/ -n Server-Cert

# install CA in SLAPD certstore
certutil -d /etc/dirsrv/slapd-syscid/ -A -n "SysCid CA" -t CT,, -a -i ca.crt 

# check SLAPD certstore
# should show Server-Cert and Syscid CA (the comments assigned in the above two imports)
certutil -d /etc/dirsrv/slapd-syscid/ -L

# install CA locally
ln -s /etc/pki/CA/ca.crt /etc/pki/trust/anchors/syscid-ca.crt
update-ca-certificates 

# start
# asks for NSS DB store password if one was set
dsctl syscid start

# check
dsctl syscid status
```

</div>

</div>
