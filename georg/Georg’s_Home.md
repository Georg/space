# Georg’s Home

<div class="contentLayout2">

<div class="columnLayout single" layout="single">

<div class="cell normal" data-type="normal">

<div class="innerCell">

Contact: georg@lysergic.dev  

  

</div>

</div>

</div>

<div class="columnLayout two-right-sidebar" layout="two-right-sidebar">

<div class="cell normal" data-type="normal">

<div class="innerCell">

<div class="recently-updated recently-updated-concise">

## Recently Updated

<div class="hidden parameters">

</div>

<div class="results-container">

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [Leon: Apache Reverse Proxy](Leon_Apache_Reverse_Proxy "Georg")
    <div class="update-item-meta">

    yesterday at 11:35 PM • updated by
    <a href="/display/~georg" class="url fn">Georg</a> •
    <a href="../georg/Leon_Apache_Reverse_Proxy" class="changes-link">view change</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [Drafts](Drafts "Georg")
    <div class="update-item-meta">

    yesterday at 11:24 PM • created by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [Georg’s Home](Georg’s_Home "Georg")
    <div class="update-item-meta">

    yesterday at 11:24 PM • updated by
    <a href="/display/~georg" class="url fn">Georg</a> •
    <a href="../georg/Georg’s_Home" class="changes-link">view change</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [sudoers2ldif.pl](/display/~georg/LDAP%3A+Sudo?preview=%2F5341350%2F5341355%2Fsudoers2ldif.pl)
    <div class="update-item-meta">

    yesterday at 11:16 PM • attached by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [LDAP: Sudo](LDAP_Sudo "Georg")
    <div class="update-item-meta">

    yesterday at 11:16 PM • updated by
    <a href="/display/~georg" class="url fn">Georg</a> •
    <a href="../georg/LDAP_Sudo" class="changes-link">view change</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [SUDOers_OU.png](/display/~georg/LDAP%3A+Sudo?preview=%2F5341350%2F5341353%2FSUDOers_OU.png)
    <div class="update-item-meta">

    yesterday at 11:15 PM • attached by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [SUDOers_Defaults.png](/display/~georg/LDAP%3A+Sudo?preview=%2F5341350%2F5341352%2FSUDOers_Defaults.png)
    <div class="update-item-meta">

    yesterday at 11:15 PM • attached by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [389 Directory Server + CA](389_Directory_Server_+_CA "Georg")
    <div class="update-item-meta">

    yesterday at 2:54 AM • updated by
    <a href="/display/~georg" class="url fn">Georg</a> •
    <a href="../georg/389_Directory_Server_+_CA" class="changes-link">view change</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [Notes](/display/~georg/Notes?focusedCommentId=5341309#comment-5341309)
    <div class="update-item-meta">

    Aug 04, 2021 • commented by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [Notes](Notes "Georg")
    <div class="update-item-meta">

    Aug 04, 2021 • created by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [Georg’s Home](../georg/Georg’s_Home)
    <div class="update-item-meta">

    Aug 04, 2021 • commented by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [Georg’s Home](../georg/Georg’s_Home)
    <div class="update-item-meta">

    Aug 04, 2021 • commented by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [Georg’s Home](../georg/Georg’s_Home)
    <div class="update-item-meta">

    Aug 04, 2021 • commented by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

-   <div class="update-item-icon">

    </div>

    <div class="update-item-content">

    [Georg](index "Georg")
    <div class="update-item-meta">

    Aug 04, 2021 • created by
    <a href="/display/~georg" class="url fn">Georg</a>

    </div>

    </div>

</div>

</div>

</div>

</div>

<div class="cell aside" data-type="aside">

<div class="innerCell">

## Navigate space

<div id="pagetreesearch">

</div>

<div class="plugin_pagetree">

</div>

</div>

</div>

</div>

<div class="columnLayout single" layout="single">

<div class="cell normal" data-type="normal">

<div class="innerCell">

  

</div>

</div>

</div>

</div>

<div class="pageSectionHeader">

## Comments:

</div>

<table data-border="0" width="100%">
<colgroup>
<col style="width: 100%" />
</colgroup>
<tbody>
<tr class="odd">
<td><p>blabla</p>
<div class="smallfont" data-align="left" style="color: #666666; width: 98%; margin-bottom: 10px;">
<img src="images/icons/contenttypes/comment_16.png" width="16" height="16" /> Posted by georg at Aug 04, 2021 01:08
</div></td>
</tr>
<tr class="even">
<td style="border-top: 1px dashed #666666"><p>test</p>
<div class="smallfont" data-align="left" style="color: #666666; width: 98%; margin-bottom: 10px;">
<img src="images/icons/contenttypes/comment_16.png" width="16" height="16" /> Posted by georg at Aug 04, 2021 01:18
</div></td>
</tr>
<tr class="odd">
<td style="border-top: 1px dashed #666666"><p>test</p>
<div class="smallfont" data-align="left" style="color: #666666; width: 98%; margin-bottom: 10px;">
<img src="images/icons/contenttypes/comment_16.png" width="16" height="16" /> Posted by georg at Aug 04, 2021 01:58
</div></td>
</tr>
</tbody>
</table>
